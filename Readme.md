# springboottest

### Proposito

Esto es la solución de la prueba técnica para postular al cargo de desarrollador full stack en Cleverit

[Prueba](https://dev.azure.com/cleveritcl/Cleverit%20Test/_wiki/wikis/Cleverit-Test.wiki/130/Desarrollador-Full-Stack-Java)

Esta solución fue desarrollada por: Marco Cerda Veas
Correo: marco.cerda.veas@gmail.com


### Software requerido

- maven 3.6.* 
- java 8
- git version 2.15.0
- docker 19.03.8

### Ejecutar en local
```bash
mvn clean install -Dmaven.test.skip=true
mvn springboot:run
```
## Ejercicio 1
Para este ejecicio se creó el controller UserController y para la interfaz se utilizó Thymeleaf.
También se creó el servicio UserService que se encarga de hacer el llamado a la API para listar, obtener, crear, actualizar y eliminar usuarios.

En esta primera etapa se crearon el listar, crear y editar en páginas distintas.


## Ejercicio 2
Para la solución de este ejercicio se utilizó una base de dato en mysql con docker.
El script con la configuraciòn de la base de dato está en la ruta.

```bash
documentos/script.sql
```

Para usar mysql con docker se hizo lo siguiente:

```bash
docker pull mysql:5
docker run -p 3306:3306 --name prueba-mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5 
docker exec -it prueba-mysql bash
root@d9058506323f:/# mysql
```

Los properties de configuración

```properties
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://localhost:3306/cleverit_db
spring.datasource.username=root
spring.datasource.password=123456
spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect
spring.datasource.driverClassName=com.mysql.jdbc.Driver
```

Se creó un controller para guardar los datos localhost:8080/licensePlate

## Ejercicio 3
Para este ejercicio se copió el archvo html list_usuario en list_usuario_ajax donde se adjuntó un javascript para hacer los llamados ajax y hoja de estilo para crear un modal.
Por tiempo solo se agregó una función para el llamado GET de la lista en main.js y se agregaron algunos estilos para lo que serìa el modal y esas cosas en style.css

## Ejercicio 4
Por tiempo no pude terminar solo dediqué 6 horas aprox para responder





