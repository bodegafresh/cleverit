CREATE DATABASE cleverit_db;
CREATE USER 'cleverit_user'@'localhost' IDENTIFIED BY '123456';
GRANT ALL PRIVILEGES ON *.* TO 'cleverit_user'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP ON cleverit_db.* TO 'cleverit_user'@'localhost';
