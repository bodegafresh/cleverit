package com.cleverit.springboottest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.cleverit.springboottest.model.data.LicensePlate;
import com.cleverit.springboottest.model.repository.LicensePlateRepository;
import com.cleverit.springboottest.service.LicensePlateService;

import lombok.extern.log4j.Log4j2;

@Controller
@Log4j2
public class LicensePlateController {

	@Autowired
	private LicensePlateService licensePlateService;

	@Autowired
	private LicensePlateRepository licensePlateRepository;

	@GetMapping("/licensePlate")
	public String index(Model model) {
		try {
			List<LicensePlate> licensePlateList = licensePlateService.getLicensePlateList();
			licensePlateList.forEach(element -> licensePlateRepository.save(element));
			model.addAttribute("dataSaved", licensePlateList.size());

			return "license_plate";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			model.addAttribute("error", e.getMessage());
		}
		return "error";
	}
}
