package com.cleverit.springboottest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cleverit.springboottest.request.user.User;
import com.cleverit.springboottest.service.UserService;

import lombok.extern.log4j.Log4j2;

@Controller
@Log4j2
public class UserController {

	private static final String REDIRECT = "redirect:/";
	private static final String ERROR = "error";

	@Autowired
	private UserService userService;

	@GetMapping("/")
	public String index(Model model) {
		try {
			List<User> usuarios = userService.getUsersList();
			model.addAttribute("listUsers", usuarios);

			return "list_usuarios";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			model.addAttribute(ERROR, e.getMessage());
		}
		return ERROR;
	}

	@GetMapping("/new")
	public String createUser(Model model) {
		try {
			User user = new User();
			model.addAttribute("user", user);
			return "nuevo_usuario";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			model.addAttribute(ERROR, e.getMessage());
		}
		return ERROR;
	}

	@GetMapping("/edit/{id}")
	public ModelAndView updateUser(@PathVariable("id") String id) {
		ModelAndView mav = new ModelAndView("editar_usuario");
		User user = userService.getUser(id);
		mav.addObject("user", user);

		return mav;
	}

	@GetMapping("/delete/{id}")
	public String removeUser(@PathVariable("id") String id) {
		try {
			User user = userService.getUser(id);
			userService.deleteUser(user);

			return REDIRECT;
		} catch (Exception e) {
			log.error(e.getMessage(), e);

		}
		return ERROR;
	}

	@PostMapping("/guardar")
	public String saveNewUser(@ModelAttribute("user") User user) {
		try {
			userService.createUser(user);
			return REDIRECT;
		} catch (Exception e) {
			log.error(e.getMessage(), e);

		}
		return ERROR;
	}

	@PostMapping("/guardarEdit")
	public String saveEditUser(@ModelAttribute("user") User user) {
		try {
			userService.updateUser(user);
			return REDIRECT;
		} catch (Exception e) {
			log.error(e.getMessage(), e);

		}
		return ERROR;
	}

	@GetMapping(value = "/ajax",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public String indexList(Model model) {
		try {
			List<User> usuarios = userService.getUsersList();
			model.addAttribute("listUsers", usuarios);

			return "list_usuarios_ajax";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			model.addAttribute(ERROR, e.getMessage());
		}
		return ERROR;
	}
	
	@GetMapping(value = "/userList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public List<User> indexList() {
		try {
			
			return userService.getUsersList();

		} catch (Exception e) {
			log.error(e.getMessage(), e);

		}
		return null;
	}

}
