package com.cleverit.springboottest.model.data;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class LicensePlate {
	@Id
	private String id;
	private String patente;
	private String tipoAuto;
	private String color;
	private String test;
}
