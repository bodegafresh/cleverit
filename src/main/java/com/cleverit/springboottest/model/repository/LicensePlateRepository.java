package com.cleverit.springboottest.model.repository;

import org.springframework.data.repository.CrudRepository;

import com.cleverit.springboottest.model.data.LicensePlate;

public interface LicensePlateRepository extends CrudRepository<LicensePlate, String> {

}
