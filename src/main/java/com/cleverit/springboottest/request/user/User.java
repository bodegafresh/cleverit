package com.cleverit.springboottest.request.user;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String apellido;
	private String email;
	private String nombre;
	private String profesion;

	@JsonInclude(value = Include.NON_NULL)
	private String id;

}
