package com.cleverit.springboottest.service;

import java.util.List;

import com.cleverit.springboottest.model.data.LicensePlate;

public interface LicensePlateService {
	
	public List<LicensePlate> getLicensePlateList();

}
