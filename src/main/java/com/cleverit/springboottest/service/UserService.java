package com.cleverit.springboottest.service;

import java.util.List;

import com.cleverit.springboottest.request.user.User;

public interface UserService {
	
	public List<User> getUsersList();
	
	public User getUser(String id);
	
	public User createUser(User user);
	
	public User updateUser(User user);
	
	public void deleteUser(User user);

}
