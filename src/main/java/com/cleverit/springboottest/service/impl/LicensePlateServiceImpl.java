package com.cleverit.springboottest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cleverit.springboottest.model.data.LicensePlate;
import com.cleverit.springboottest.service.LicensePlateService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class LicensePlateServiceImpl implements LicensePlateService {
	
	@Value("${com.cleverit.licenseplate.url}")
	private String url;
	
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public List<LicensePlate> getLicensePlateList() {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode dataList = restTemplate.exchange(url, HttpMethod.GET, null, JsonNode.class).getBody();
		return mapper.convertValue(
				dataList, 
			    new TypeReference<List<LicensePlate>>(){}
			);
		
	}

}
