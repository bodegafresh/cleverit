package com.cleverit.springboottest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cleverit.springboottest.request.user.User;
import com.cleverit.springboottest.service.UserService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class UserServiceImpl implements UserService {

	@Value("${com.cleverit.user.url}")
	private String url;
	
	private RestTemplate restTemplate = new RestTemplate();

	
	@Override
	public List<User> getUsersList() {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode users = restTemplate.exchange(url, HttpMethod.GET, null, JsonNode.class).getBody();
		return mapper.convertValue(
			    users, 
			    new TypeReference<List<User>>(){}
			);

	}

	@Override
	public User getUser(String id) {
		String realUrl = url + "/" + id;
		return restTemplate.exchange(realUrl, HttpMethod.GET, null, User.class).getBody();
	}

	@Override
	public User createUser(User user) {
		HttpEntity<User> entity = new HttpEntity<>(user);
		return restTemplate.exchange(url, HttpMethod.POST, entity, User.class).getBody();
	}

	@Override
	public User updateUser(User user) {
		HttpEntity<User> entity = new HttpEntity<>(user);
		String realUrl = url + "/" + user.getId(); 
		log.info(realUrl);
		return restTemplate.exchange(realUrl, HttpMethod.PUT, entity, User.class).getBody();
	}

	@Override
	public void deleteUser(User user) {
		String realUrl = url + "/" + user.getId();
		restTemplate.exchange(realUrl, HttpMethod.DELETE, null, Void.class);
	
	}

}
