function callGetAjax(url, callback) {
	let xmlhttp;
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			callback(xmlhttp.responseText);
		}
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function callPostAjax(url, data, callback) {
	let xmlhttp;
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 201) {
			callback(xmlhttp.responseText);
		} else {
			console.log(xmlhttp.status);
		}
	}
	xmlhttp.open("POST", url, true);
	xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xmlhttp.send(JSON.stringify(data));
}

function callPutAjax(url, data, callback) {
	let xmlhttp;
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			callback(xmlhttp.responseText);
		} else {
			console.log(xmlhttp.status);
		}
	}
	xmlhttp.open("PUT", url, true);
	xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xmlhttp.send(JSON.stringify(data));
}

function callDeleteAjax(url, callback) {
	let xmlhttp;
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			callback(xmlhttp.responseText);
		} else {
			console.log(xmlhttp.status);
		}
	}
	xmlhttp.open("DELETE", url, true);
	xmlhttp.send();
}

function llenaTabla(resultado) {

	let jsonList = JSON.parse(resultado);
	let table = document.getElementById('tableList');
	table.innerHTML = "";
	for (let i = 0; i < jsonList.length; i++) {

		let tr = document.createElement('tr');
		let s = jsonList[i];

		let td = document.createElement('td');
		td.innerHTML = s.id;
		tr.appendChild(td);

		td = document.createElement('td');
		td.innerHTML = s.nombre;
		tr.appendChild(td);

		td = document.createElement('td');
		td.innerHTML = s.apellido;
		tr.appendChild(td);

		td = document.createElement('td');
		td.innerHTML = s.email;
		tr.appendChild(td);

		td = document.createElement('td');
		td.innerHTML = s.profesion;
		tr.appendChild(td);

		td = document.createElement('td');
		let btn = document.createElement('input');
		btn.type = "button";
		btn.className = "edit";
		btn.value = 'Editar';
		btn.onclick = (function(entry) { return function() { editarUsuario(s.id); } })(s.id);
		td.appendChild(btn);

		btn = document.createElement('input');
		btn.type = "button";
		btn.className = "delete";
		btn.value = 'Eliminar';
		btn.onclick = (function(entry) { return function() { eliminarUsuario(s.id); } })(s.id);
		td.appendChild(btn);
		tr.appendChild(td);

		table.appendChild(tr);
	}
}

function editarUsuario(id) {
	let url = 'http://arsene.azurewebsites.net/User/' + id;
	callGetAjax(url, showEditUser);
}

function showEditUser(user) {
	let realUser = JSON.parse(user);
	let modalUsuario = document.getElementById("modalUsuario");
	let btnGuardar = document.getElementById("btnGuardar");
	btnGuardar.onclick = (function(entry) { return function() { guardarEditado('editado'); } })('editado');
	modalUsuario.style.visibility = (modalUsuario.style.visibility == "visible") ? "hidden" : "visible";
	if (modalUsuario.style.visibility == "visible") {

		let userId = document.getElementById("inputId");
		let nombre = document.getElementById("inputNombre");
		let apellido = document.getElementById("inputApellido");
		let email = document.getElementById("inputEmail");
		let profesion = document.getElementById("inputProfesion");

		userId.value = realUser.id;
		nombre.value = realUser.nombre;
		apellido.value = realUser.apellido;
		email.value = realUser.email;
		profesion.value = realUser.profesion;

		modalUsuario.style.display = "inline-block";
	} else {
		modalUsuario.style.display = "none";
	}

}

function eliminarUsuario(id) {
	var respuesta = confirm("Eliminar usuario?!");
	if (respuesta) {
		let url = 'http://arsene.azurewebsites.net/User/' + id;
		callDeleteAjax(url, showEliminarUser);
	} 
}

function showEliminarUser(user) {
	console.log(user);
	alert("Usuario eliminado");
	callGetAjax('http://arsene.azurewebsites.net/User', llenaTabla);
}

function crearUsuario() {
	let modalUsuario = document.getElementById("modalUsuario");
	let btnGuardar = document.getElementById("btnGuardar");
	btnGuardar.onclick = (function(entry) { return function() { guardarNuevo('nuevo'); } })('nuevo');
	modalUsuario.style.visibility = (modalUsuario.style.visibility == "visible") ? "hidden" : "visible";
	if (modalUsuario.style.visibility == "visible") {

		let userId = document.getElementById("inputId");
		let nombre = document.getElementById("inputNombre");
		let apellido = document.getElementById("inputApellido");
		let email = document.getElementById("inputEmail");
		let profesion = document.getElementById("inputProfesion");

		userId.value = undefined;
		nombre.value = "";
		apellido.value = "";
		email.value = "";
		profesion.value = "";

		modalUsuario.style.display = "inline-block";
	} else {
		modalUsuario.style.display = "none";
	}
}

function guardarEditado(lugar) {
	console.log(lugar);
	let idUser = document.getElementById("inputId");
	let nombre = document.getElementById("inputNombre");
	let apellido = document.getElementById("inputApellido");
	let email = document.getElementById("inputEmail");
	let profesion = document.getElementById("inputProfesion");

	let user = {
		id: idUser.value,
		nombre: nombre.value,
		apellido: apellido.value,
		email: email.value,
		profesion: profesion.value
	}

	callPutAjax('http://arsene.azurewebsites.net/User/' + idUser.value, user, usuarioGuardado);
}

function guardarNuevo(lugar) {
	console.log(lugar);
	let nombre = document.getElementById("inputNombre");
	let apellido = document.getElementById("inputApellido");
	let email = document.getElementById("inputEmail");
	let profesion = document.getElementById("inputProfesion");

	let user = {
		nombre: nombre.value,
		apellido: apellido.value,
		email: email.value,
		profesion: profesion.value
	}

	callPostAjax('http://arsene.azurewebsites.net/User', user, usuarioGuardado);
}

function usuarioGuardado(datos) {
	console.log(datos);
	hiddenModal();
	callGetAjax('http://arsene.azurewebsites.net/User', llenaTabla);
}

function hiddenModal() {

	let modalUsuario = document.getElementById("modalUsuario");
	console.log(modalUsuario.style.visibility);
	modalUsuario.style.visibility = (modalUsuario.style.visibility == "visible") ? "hidden" : "visible";
	if (modalUsuario.style.visibility == "visible") {
		modalUsuario.style.display = "inline-block";
	} else {
		modalUsuario.style.display = "none";
	}
}

callGetAjax('http://arsene.azurewebsites.net/User', llenaTabla);

